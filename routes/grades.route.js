const express = require('express');
const router = express.Router();

const controller = require('../controllers/grades.controller'); 

router.route('/')
    .get(controller.getGrades)
    .post(controller.createGrade);

router.route('/:gradeId')
    .get(controller.getGrade)
    .patch(controller.updateGrade)
    .delete(controller.deleteGrade);

module.exports = router;
